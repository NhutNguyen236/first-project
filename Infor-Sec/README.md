# Watermark embedding for DNN

## Table of contents

- Neural Network
- Artificial Neural Network - ANN
- Deep Neural Network - DNN
- Embedding watermark into DNN 

##  Biological Neural Network

<p>
    <img src = "https://i.ibb.co/Ttqf1nT/image.png"/>
</p>

- Our brain has over 10^11 neurons with over 10^14 connections

## Artificial Neural Network - ANN

- Coming soon 

## Deep Neural Network - DNN

- Coming soon

## Embedding watermark into DNN 

### Watermarking definition 
### Why we should watermark our DNN model

- To protect your authentication towards your model
- To help keep the community clean from plagiarism
- To help keep the plagiarised MLaaS away from users

### Types of attack

- There are many ways to attack your DNN model
- But most of them, we are again discussing about watermarking so attacking here means stealing

### Embedding Algorithms

<img src = "https://i.ibb.co/dQPYsdj/image.png"/>

Since a Deep neural network or a single neural network still performs the same thing, still have 3 main layers - input, hidden and output and the coolest part is the hidden one since its function is to intervene between the external input and the network output and its ouput vector Y(x) should be

<p align = "center">
    <img src = "https://i.ibb.co/YBh18rH/image.png" width = "300"/>
</p>

<img src = "https://i.ibb.co/mX8X05V/image.png"/>

- Now we know what our model is  functioning, it's time to protect it, watermarking time
- There are 2 ways to approach the target, white box and black box

#### Whitebox approach
- Let's go for the `whitebox` first: we are going to use a transformation matrix to perform watermark embedding
## References

- How to get your materials 
    - [WorldCat](https://www.worldcat.org/)
- How to download article 
    